import argparse
import sys
from argparse import Namespace

import lib.command_callbacks


def configure_args(binary_filename):
    """@todo split arguemnts/options configuration to command areas"""
    cli_root = argparse.ArgumentParser(prog=binary_filename, description='SFCC sandboxes CI runner.')
    cli_root.version = lib.command_callbacks.version
    cli_root.add_argument('--version', help='Show version.', action='version')
    main_commands = cli_root.add_subparsers(
        title='Commands',
        # description='Main Commands',
        dest='command',
    )
    main_commands.add_parser(
        'list',
        add_help=True,
        description='List defined sandboxes.',
        help='List defined sandboxes.'
    )
    jobs_command = main_commands.add_parser(
        'jobs',
        add_help=True,
        description="List available CI jobs.",
        help='List available CI jobs (filtered by "SBX" key string. Use all to get all.'
    )
    jobs_command.add_argument('--all', help='List all jobs.', action='store_true')
    main_commands.add_parser(
        'config:dump',
        add_help=True,
        description='Dump configuration data.',
        help='Dump configuration data.'
    )
    main_commands.add_parser(
        'config:sample',
        add_help=True,
        description='Create sample configuration files.',
        help='Create sample configuration files.'
    )
    main_commands.add_parser(
        'config:files',
        add_help=True,
        description='Show list of configuration files.',
        help='Show list of configuration files.'
    )
    sb_command = main_commands.add_parser(
        'sb',
        add_help=True
        , help="Manage sandbox."
    )
    sb_command.add_argument(
        'sandbox_key', metavar='SANDBOX_KEY', type=str,
        help='Sandbox key.', default='list', choices=['omc', 'half'])
    sb_actions = sb_command.add_subparsers(title='Sandbox actions', dest='sandbox_action')
    sb_actions.add_parser('start', add_help=True, help='Start sandbox')
    sb_actions.add_parser('stop', add_help=True, help='Start sandbox')
    sb_actions.add_parser('restart', add_help=True, help='Start sandbox')
    sb_actions.add_parser('ref', add_help=True, prog='ref', help='Open sandbox reference.') \
        .add_argument('reference', metavar='REF_KEY')

    return cli_root


def _force_arugments():
    """use `list` for empty arguments."""
    if len(sys.argv) == 1:
        sys.argv.append('list')

    """use `sb` (sandbox) non-command arguments."""
    if len(sys.argv) > 1 and sys.argv[1] not in (
            '-h',
            '--help',
            '--version',
            'list',
            'sb',
            'jobs',
            'config:sample',
            'config:files',
            'config:dump'):
        sys.argv.insert(1, 'sb')


def parse_args(cli_app) -> Namespace:
    _force_arugments()
    return cli_app.parse_args()


def command_callback():
    """@todo move callbacks from this file """
    return {
        'sb': lib.command_callbacks.sandbox_action,
        'jobs': lib.command_callbacks.print_jobs,
        'list': lib.command_callbacks.print_sandboxes,
        'config:sample': lib.command_callbacks.set_sample_config,
        'config:dump': lib.command_callbacks.print_config_dump,
        'config:files': lib.command_callbacks.print_config_files,
    }


def parse_and_exec(binary_filename):
    args = parse_args(
        configure_args(binary_filename)
    )
    return command_callback()[args.command](args)
