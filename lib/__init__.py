import sys
import os

modules_dir = os.path.dirname(os.path.abspath(__file__ + '/../')) + '/project-modules'

if os.path.exists(modules_dir):
    sys.path.insert(
        0, modules_dir
    )
