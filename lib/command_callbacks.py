import time
from argparse import Namespace

from lib.sandbox_ci import SandboxCi
from lib import config
import sys
import os

from lib.print_error import print_error

__conf: dict = config.read(os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../'))
__sandbox = SandboxCi(__conf)


def sandbox():
    """
    :rtype: SandboxCi
    """
    global __sandbox

    return __sandbox


def configuration():
    global __conf
    """
    :rtype: dict
    """
    return __conf


# API
def sandbox_action(input_args: Namespace):
    """Start/stop SFCC sandbox via CI"""

    sandbox().key = input_args.sandbox_key

    if input_args.sandbox_action == 'ref':
        _open_reference(input_args.reference)
        return

    job_url = _call_sandbox(input_args.sandbox_action)
    print('Started job URL: %s' % job_url)
    sys.stdout.flush()
    if configuration()['cli']['open_build_in_browser']:
        _open_build_url(job_url)


def _open_reference(reference):
    _open_sandbox_url(reference)


def _open_build_url(job_url, delay_sec=6):
    import time
    print('Waiting for redirection for %ss...' % delay_sec)
    sys.stdout.flush()
    # wait for starting a job
    time.sleep(delay_sec)
    _open_url(job_url)


def _open_url(url):
    import webbrowser
    webbrowser.open(url, new=0, autoraise=True)


def _call_sandbox(action):
    if action == 'start':
        return sandbox().start()
    elif action == 'stop':
        return sandbox().stop()
    elif action == 'restart':
        return sandbox().restart()
    else:
        raise Exception('Unknown action "%s".' % action)


# API
def print_sandboxes(input_args: Namespace):
    from prettytable import PrettyTable
    table = PrettyTable()
    table.field_names = ["Key", "ID", "Description"]
    table.align['Key'] = 'l'
    table.align['Description'] = 'l'
    for i in configuration()['sandbox']:
        table.add_row([
            i,
            configuration()['sandbox'][i]['id'] if 'id' in configuration()['sandbox'][i]
            else '--',
            configuration()['sandbox'][i]['description'] if 'description' in configuration()['sandbox'][i]
            else '--'
        ])
    print(table)


# API
def print_jobs(input_args: Namespace):
    for ci_job in sandbox().ci.jobs(input_args.all):
        print(ci_job)


# API
def print_config_dump(input_args: Namespace):
    from pprint import pprint

    if configuration()['api']['login']['token']:
        # hide password
        configuration()['api']['login']['token'] = '****'

    pprint(configuration())


# API
def print_config_files(input_args: Namespace):
    import lib.config as config

    # init config
    configuration()

    print('Configuration use paths:')
    print('- ' + "\n- ".join(config.paths_to_load()))
    print('Configuration loaded files:')
    print('- ' + "\n- ".join(config.loaded_files()))


# API
def set_sample_config(input_args: Namespace):
    import lib.config as config

    files = config.set_sample_config(_root_dir())
    if files:
        print('Created sample config files:')
        for i in files:
            print(' - ' + i)
        print('Please update them with own data.')
    else:
        print('notice: You have already configuration files.', file=sys.stderr)


# API
def print_help(input_args: Namespace):
    import lib.config as config
    if config.is_binary():
        binary = './sbx.exe'
        config_name = config.binary_config_basename()
    else:
        binary = 'python sandbox.py'
        config_name = config.config_basename()

    print(
        open(_root_dir() + '/help-cli.txt', 'r').read().replace('{bin}', binary).replace('{config_name}', config_name))


# API
def print_version(input_args: Namespace):
    print(version())


# API
def version():
    return open(_root_dir() + '/.version', 'r').read()


def _root_dir():
    return os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../')


def is_binary():
    """
    Check if scripts ran in binary file
    :rtype: bool
    """
    return hasattr(sys, '_MEIPASS')


def _open_sandbox_url(reference: str):
    if 'ref' not in sandbox().sandbox_info() or reference not in sandbox().sandbox_info()['ref']:
        raise Exception(
            'There is no declared URL by path in configuration: sandbox.{}.ref.{}'.format(sandbox().key, reference))

    print('Sending URL to your system browser...')
    print(sandbox().sandbox_info()['ref'][reference])
    sys.stdout.flush()
    time.sleep(1)

    _open_url(
        sandbox().sandbox_info()['ref'][reference]
    )
