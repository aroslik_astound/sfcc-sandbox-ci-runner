# service
import os
import sys
import yaml
from pathlib import Path
import glob

from deepmerge import Merger

__config_files = []
__config_paths = []

config_merger = Merger(
    # pass in a list of tuple, with the
    # strategies you are looking to apply
    # to each type.
    [
        (list, ["append"]),
        (dict, ["merge"])
    ],
    # next, choose the fallback strategies,
    # applied to all other types:
    ["override"],
    # finally, choose the strategies in
    # the case where the types conflict:
    ["use_existing"]
)


# API
def paths_to_load():
    return __config_paths


# API
def loaded_files():
    return __config_files


# API
def read(base_path: str = None):
    if not base_path:
        base_path = os.path.dirname(__file__)

    configuration = initial_config()

    # read config.yml.dist
    add_default_config(configuration, base_path)
    if is_binary():
        add_binary_config(configuration)
    else:
        # read custom config.yml
        add_config(configuration, base_path)

    # read custom ~/.sbxrc
    add_user_config(configuration)

    return configuration


def add_default_config(configuration, base_path):
    __config_paths.append('%s/%s' % (base_path, 'config.yml.dist'))
    if os.path.exists(base_path + '/config.yml.dist'):
        for filepath in Path(base_path).glob('config.yml.dist'):
            config_merger.merge(configuration, yaml.safe_load(open(str(filepath), 'r')))
            __config_files.append(str(filepath))


def add_user_config(configuration):
    user_file = configuration['config']['user_file'].replace('~', str(Path.home()))
    user_filename = os.path.basename(user_file)
    user_dir = os.path.dirname(user_file)
    __config_paths.append('%s/%s' % (user_dir, user_filename))
    if glob.glob1(user_dir, user_filename):
        for filepath in Path(user_dir).glob(user_filename):
            with open(str(filepath), 'r') as file_stream:
                config_merger.merge(configuration, yaml.safe_load(file_stream))
                __config_files.append(str(filepath))


def add_config(configuration, base_path):
    __config_paths.append('%s/%s' % (base_path, config_basename()))
    if glob.glob1(base_path, config_basename()):
        for filepath in Path(base_path).glob(config_basename()):
            with open(str(filepath), 'r') as file_stream:
                config_merger.merge(configuration, yaml.safe_load(file_stream))
                __config_files.append(str(filepath))


def config_basename():
    return 'config*.yml'


def add_binary_config(configuration):
    # TODO this works only Windows binary files
    """
    Use the same path as for executable file.
    Also use the same file basename.
    `/path/to/myfile.exe` => `/path/to/myfile*.yml`
    """
    basename = binary_config_basename()
    bin_path = os.path.dirname(sys.executable)

    __config_paths.append('%s/%s' % (bin_path, basename))

    if glob.glob1(bin_path, basename):
        for filepath in Path(bin_path).glob(basename):
            with open(str(filepath), 'r') as file_stream:
                config_merger.merge(configuration, yaml.safe_load(file_stream))
                __config_files.append(str(filepath))


def binary_config_basename():
    return os.path.basename(sys.executable.replace('.exe', '*.yml'))


def initial_config():
    return {
        'api':
            {
                # CI authorization credentials
                'login':
                    {
                        'username': '',
                        # API token
                        'token': '',
                    },

                'url': '',
            },
        'sandbox': {},
        'cli': {
            'open_build_in_browser': True
        },
        'debug': False
    }


def is_binary():
    """
    Check if scripts ran in binary file
    :rtype: bool
    """
    return hasattr(sys, '_MEIPASS')


def set_sample_config(root):
    from shutil import copyfile

    created = []

    if is_binary():
        if not glob.glob1(os.path.dirname(sys.executable), os.path.basename(binary_config_basename())):
            copyfile(root + '/config.yml.sample', binary_config_basename().replace('*', ''))
            created.append(binary_config_basename().replace('*', ''))
    else:
        if not glob.glob1(root, 'config*.yml'):
            copyfile(root + '/config.yml.sample', root + '/config.yml')
            created.append(root + '/config.yml')

    if not created:
        return []

    user_config = '~/.sbxrc'.replace('~', str(Path.home()))
    if not os.path.exists(user_config):
        copyfile(root + '/.sbxrc.sample', user_config)
        created.append(user_config)

    return created
