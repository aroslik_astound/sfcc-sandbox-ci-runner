import os
import sys
from typing import Dict

from lib import config
from lib.ci_client import CiClient


class SandboxCi:
    __key: str
    conf: dict = None
    ci_client: CiClient = None

    def __init__(self, configuration: dict, key=None):
        self.__key = key
        self.conf = configuration

    @property
    def key(self):
        """
        :rtype: str
        :return:
        """
        return self.__key

    @key.setter
    def key(self, value):
        self.__key = str(value)

    @property
    def id(self):
        """
        Get sandbox identifier
        :rtype: str
        """
        if 'id' not in self.sandbox_info():
            raise Exception('There is no such sandbox id "%s". Please check your config.yml file.' % self.key)
        return str(self.sandbox_info()['id'])

    def sandbox_info(self):
        if not self.key:
            raise Exception('Sandbox key is not provided.')
        if self.key not in self.configuration['sandbox']:
            raise Exception('There is no such sandbox "%s" declaration. Please check your config.yml file.' % self.key)
        return self.configuration['sandbox'][self.key]

    @property
    def ci(self):
        if not self.ci_client:
            self.ci_client = CiClient(
                url=self.configuration['api']['url'],
                username=self.configuration['api']['login']['username'],
                token=self.configuration['api']['login']['token'],
                sandbox_jobs=self.configuration['api']['sandbox_job']
            )
        return self.ci_client

    @property
    def configuration(self):
        """

        :rtype: dict
        """
        if not self.conf:
            self.conf = config.read(
                base_path=os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../')
            )
        return self.conf

    def start(self):
        return self.ci.start_sandbox(sandbox_id=self.id)

    def restart(self):
        return self.ci.restart_sandbox(sandbox_id=self.id)

    def stop(self):
        return self.ci.stop_sandbox(sandbox_id=self.id)
