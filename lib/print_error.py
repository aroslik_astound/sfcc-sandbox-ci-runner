import sys


def print_error(call_func, *args, **kwargs):
    """ Wrap whole execution and print exceptions """

    def wrapper():
        try:
            call_func(*args, **kwargs)
        except Exception as e:
            if print_error.__debug:
                import traceback
                print('trace: ' + traceback.format_exc(), file=sys.stderr)
                sys.exit(2)
            else:
                print('error: [{}] {}'.format(str(e.__class__.__name__), str(e)), file=sys.stderr)
                sys.exit(2)

    return wrapper


print_error.__debug = False
