import re
import sys

from jenkinsapi.jenkins import Jenkins
from jenkinsapi.job import Job
from urllib3.exceptions import ReadTimeoutError


class CiClient:
    jobs_list = None
    client: Jenkins
    sandbox_jobs: dict
    JOB_KEY_CODE = 'SBX'

    def __init__(self, url, sandbox_jobs: dict, username=None, token=None):
        self.client = Jenkins(baseurl=url, username=username, password=token)
        self.sandbox_jobs = sandbox_jobs

    def get_job_name(self, action):
        return self.sandbox_jobs[action]

    def has_job(self, job_name):
        return job_name in self.jobs()

    def jobs(self, all_jobs=False):
        if not self.jobs_list:
            self.jobs_list: list = self.client.get_jobs_list()

        if not all_jobs:
            return [i for i in self.jobs_list if self.JOB_KEY_CODE in i]

        return self.jobs_list

    # API method
    def start_sandbox(self, sandbox_id: str):
        return self.call_sandbox('start', sandbox_id)

    # API method
    def restart_sandbox(self, sandbox_id: str):
        return self.call_sandbox('restart', sandbox_id)

    # API method
    def stop_sandbox(self, sandbox_id: str):
        return self.call_sandbox('stop', sandbox_id)

    def call_sandbox(self, action, sandbox_id):
        import requests

        try:
            return self.start_sandbox_job(action, sandbox_id)
        except (requests.exceptions.ConnectionError, ReadTimeoutError) as e:
            print('ConnectionError: %s' % str(e), file=sys.stderr)
            print('Retrying...', file=sys.stderr)
            sys.stderr.flush()
            return self.start_sandbox_job(action, sandbox_id)

    def start_sandbox_job(self, action, sandbox_id):
        build_params = {'SANDBOX_IDS': sandbox_id}
        build_number = self.job(action).get_next_build_number()

        self.build_job(self.get_job_name(action), build_params)

        return self.get_build_url(action, build_number, build_params)

    def build_job(self, job_name, build_params):
        try:
            self.client.build_job(job_name, build_params)
        except ValueError as e:
            ##
            # Suppress ignore "Not a Queue URL" error,
            # Jenkins library or Jenkins itself is not configured properly according to
            #
            # https://github.com/pycontribs/jenkinsapi/issues/513
            # References:
            # - jenkinsapi/job.py:234
            # - jenkinsapi.job.Job.invoke()
            if 'Not a Queue URL' not in str(e):
                raise e

    def get_build_url(self, action, build_number, build_params):
        return re.sub(r'(\d+/?)$', str(build_number) + '/',
                      self.job(action).get_last_build().get_build_url())

    def job(self, sandbox_action):
        job: Job = self.client[self.get_job_name(sandbox_action)]
        return job

    @staticmethod
    # def get_queued_build(build_params, job: Job) -> QueueItem:
    def get_queued_build(build_params, job: Job):
        """
        Returns build if a build with build_params is currently queued.
        :rtype: QueueItem
        """
        queue = job.jenkins.get_queue()
        queued_builds = queue.get_queue_items_for_job(job.name)
        for build in queued_builds:
            if build.get_parameters() == build_params:
                return build
        return None
