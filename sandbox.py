import os
import sys

import lib.command_callbacks as command_callbacks
from lib.print_error import print_error
from lib.cli_configure import parse_and_exec


@print_error
def run():
    parse_and_exec(
        os.path.basename(sys.executable) if command_callbacks.is_binary()
        else os.path.basename(__file__)
    )


if __name__ == '__main__':
    run()
