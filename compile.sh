~/.python/python36/Scripts/pyinstaller.exe \
  --onefile sandbox.py \
  --paths ./project-modules \
  --add-data 'help-cli.txt;.' \
  --add-data 'README.md;.' \
  --add-data '.sbxrc.sample;.' \
  --add-data 'config.yml.sample;.' \
  --add-data 'config.yml.dist;.' \
  --add-data '.version;.'

mv ./dist/sandbox.exe ./dist/sbx.exe