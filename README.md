# SFCC Sandbox CI Runner

This tool can start/restart/stop an SFCC sandbox by using CI Jenkins job like `SBX/SBX-start`.

Also, it may help to keep links to your sandboxes and open them quickly.

## Quick Start

1. Download [dist/sbx.exe](dist/sbx.exe)
2. Run `/path/to/sbx.exe config:sample`
3. Fill your API credentials in `~/.sbxrc` (where `~` is your home directory)
4. Set your sandbox information `/path/to/sbx.yml` (the same path where your `sbx.exe` file)
    1. replace `some-key` with any your sandbox key
    2. replace `id` value with your sandbox ID (like: `SAMPLEdc-84a6-44f8-8225-a341d4SAMPLE`)
    3. replace `description` value with your sandbox information
5. Check access to `ci-master-02` and double check accessing to `SBX/` jost by command `/path/to/sbx.exe jobs`
6. Start/stop your sandbox `/path/to/sbx.exe SANDBOX_KEY start|stop|restart`


## Environment

For scripted version:

- Any OS
- Python (implemented on v3.6)
- Python `pip` package installer

For binary "exe" version:

- Windows OS

### Binary Installation

1. Download [dist/sbx.exe](dist/sbx.exe).
2. Create configuration file as described below but use base filename of executable file instead of `config`.

It will load all files by format: `{exe filename}*.yml`.
E.g. `sbx.yml` or `sbx-my.yml` if we have binary file `sbx.exe`.

It can be following for basename `sfcc_sbx`:

```
D:/sandbox/sfcc_sbx.exe
D:/sandbox/sfcc_sbx.yml
D:/sandbox/sfcc_sbx-another.yml
D:/sandbox/sfcc_sbxonemore.yml
```

### Installation

Get in the program root and run:

```shell
$ pip install -r requirements.txt --target=project-modules --upgrade
```

### Running command

```shell
$ python path/to/sandbox.py
```

where `path/to` your local path to root directory of this application.

Or, you may wrap it into an alias in your environment.
For `bash` in `~/.bashrc`:

```shell
alias sandbox='python /path/to/sandbox.py'
```

## Configuration

Declare your configuration in the file `config.yml` (you may copy `config.yml.sample`):

```yaml
sandbox:
  ab1:
    id: 'SAMPLEdc-84a6-44f8-8225-a341d4SAMPLE'
    description: >-
      Your comment

      SBX-000 (another line)
```

where

* `ab1` is your associated custom key to the sandbox (`SANDBOX_KEY`)
* `id` - real sandbox ID
* `description` - any text for description.
* `username` - your Jenkins username
* `token` - your Jenkins API token

Token can be generated in `UserAccount > Configure > API Token`.

API credentials can be declared in the same file but it's better them in the user home directory in the file `~/.sbxrc`

```yaml

api:
  login:
    username: 'u.ser'
    # token
    token: 'SAMPLE775df74ba01b943cd81c8ab110bf'
```

#### For binary

Use filename `sbx.yml` or `sbx-my.yml` in the same directory where `sbx.exe`.

### Generate configuration file/s from sample one/s
To generate sample files use
```shell
$ python sandbox.py config:sample
Created sample config files:
 - config.yml
 - C:\Users\myuser/.sbxrc
Please update them with own data.
```
and for binary:
```shell
$ ./sbx.exe config:sample
Created sample config files:
 - sbx.yml
 - C:\Users\myuser/.sbxrc
Please update them with own data.
```

## Command line
### - Show all sandboxes
```
$ python sandbox.py
+-------------+--------------------------------------+-------------+
| Key         |                  ID                  | Description |
+-------------+--------------------------------------+-------------+
| project-ONE | SAMPLEdc-84a6-44f8-8225-a341d4SAMPLE | ... ...     |
|             |                                      | SBX-000 ... |
+-------------+--------------------------------------+-------------+
```

### - Start/stop an added sandbox
To start declared `ab1` sandbox just run:
```
$ python sandbox.py ab1 start 
Started job URL: https://example.com/jenkins/job/SBX/job/SBX-start/000/
```
it will open the URL in your system browser after this print.

##### Format
```
$ python sandbox.py SANDBOX_KEY ACTION
```

where `ACTION` can be (a part of a related CI job name):

*  `start`
*  `restart`
*  `stop`

### - Generate sample configuration file
Check your accessible CI jobs list by the command:

```
$ python sandbox.py config:sample
```

### - Get configuration paths

Check your paths to configuration files:

```shell
$ sbx.exe config:files
Configuration use paths:
- C:\Users\u.ser\AppData\Local\Temp\_MEI258322/config.yml.dist
- C:\Users\u.ser\bin/sbx*.yml
- C:\Users\u.ser/.sbxrc
Configuration loaded files:
- C:\Users\u.ser\AppData\Local\Temp\_MEI258322\config.yml.dist
- C:\Users\u.ser\bin\sbx.yml
- C:\Users\u.ser\.sbxrc
```

### - Dump configuration

Check your accessible CI jobs list by the command:

```
$ python sandbox.py config:dump
{'api': {'login': {'token': '****', 'username': 'u.user'},
[...]
```

### - Open a reference URL of your sandbox in the system browser

You may declare reference URLs in your sandbox and quickly open them.

Configuration:

```yaml
sandbox:
  project-ONE:
    ref:
      bm: 'https://your-key.sandbox.us01.dx.commercecloud.salesforce.com/on/demandware.store/Sites-Site'
      sf: 'https://your-key.sandbox.us01.dx.commercecloud.salesforce.com/s/RefArchGlobal/'
      ticket: 'https://jira.ontrq.com/browse/SBX-000'
```
Open URL in your system browser:
```
$ python sandbox.py project-1 project-ONE ticket
Sending URL to your system browser...
https://jira.ontrq.com/browse/SBX-000

```

### - List all available jobs in `ci-master-02`
Check your accessible CI jobs list by the command:
```
$ python sandbox.py jobs
[ ... list of CI jobs here where you have access to ]
```

## Opening created build in a web browser

Created CI build link will appear in CLI when the application got a success response.

It will be automatically sent to your system browser so that your will open a new table for this URL.

### Disable opening in browser

To disable this option just declare following in your `config.yml` file:

```yml
cli:
  open_build_in_browser: false
```